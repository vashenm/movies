<?php

namespace App\Entity;

use App\Repository\MoviesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MoviesRepository::class)
 */
class Movies
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\ManyToMany(targetEntity=Theatres::class, inversedBy="Movies")
     */
    private $Theatre;

    /**
     * @ORM\ManyToMany(targetEntity=Times::class, mappedBy="Movies")
     */
    private $Times;

    /**
     * @ORM\OneToMany(targetEntity=Bookings::class, mappedBy="Movie")
     */
    private $Bookings;

    public function __construct()
    {
        $this->Theatre = new ArrayCollection();
        $this->Times = new ArrayCollection();
        $this->Bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    /**
     * @return Collection|Theatres[]
     */
    public function getTheatre(): Collection
    {
        return $this->Theatre;
    }

    public function addTheatre(Theatres $theatre): self
    {
        if (!$this->Theatre->contains($theatre)) {
            $this->Theatre[] = $theatre;
        }

        return $this;
    }

    public function removeTheatre(Theatres $theatre): self
    {
        if ($this->Theatre->contains($theatre)) {
            $this->Theatre->removeElement($theatre);
        }

        return $this;
    }

    /**
     * @return Collection|Times[]
     */
    public function getTimes(): Collection
    {
        return $this->Times;
    }

    public function addTime(Times $time): self
    {
        if (!$this->Times->contains($time)) {
            $this->Times[] = $time;
            $time->addMovie($this);
        }

        return $this;
    }

    public function removeTime(Times $time): self
    {
        if ($this->Times->contains($time)) {
            $this->Times->removeElement($time);
            $time->removeMovie($this);
        }

        return $this;
    }

    /**
     * @return Collection|Bookings[]
     */
    public function getBookings(): Collection
    {
        return $this->Bookings;
    }

    public function addBooking(Bookings $booking): self
    {
        if (!$this->Bookings->contains($booking)) {
            $this->Bookings[] = $booking;
            $booking->setMovie($this);
        }

        return $this;
    }

    public function removeBooking(Bookings $booking): self
    {
        if ($this->Bookings->contains($booking)) {
            $this->Bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getMovie() === $this) {
                $booking->setMovie(null);
            }
        }

        return $this;
    }
}

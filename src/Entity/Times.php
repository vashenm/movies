<?php

namespace App\Entity;

use App\Repository\TimesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TimesRepository::class)
 */
class Times
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $time;

    /**
     * @ORM\ManyToMany(targetEntity=Theatres::class, inversedBy="Times")
     */
    private $Theatre;

    /**
     * @ORM\ManyToMany(targetEntity=Movies::class, inversedBy="Times")
     */
    private $Movies;

    /**
     * @ORM\OneToMany(targetEntity=Bookings::class, mappedBy="Time")
     */
    private $Bookings;

    public function __construct()
    {
        $this->Theatre = new ArrayCollection();
        $this->Movies = new ArrayCollection();
        $this->Bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTime(): ?string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @return Collection|Theatres[]
     */
    public function getTheatre(): Collection
    {
        return $this->Theatre;
    }

    public function addTheatre(Theatres $theatre): self
    {
        if (!$this->Theatre->contains($theatre)) {
            $this->Theatre[] = $theatre;
        }

        return $this;
    }

    public function removeTheatre(Theatres $theatre): self
    {
        if ($this->Theatre->contains($theatre)) {
            $this->Theatre->removeElement($theatre);
        }

        return $this;
    }

    /**
     * @return Collection|Movies[]
     */
    public function getMovies(): Collection
    {
        return $this->Movies;
    }

    public function addMovie(Movies $movie): self
    {
        if (!$this->Movies->contains($movie)) {
            $this->Movies[] = $movie;
        }

        return $this;
    }

    public function removeMovie(Movies $movie): self
    {
        if ($this->Movies->contains($movie)) {
            $this->Movies->removeElement($movie);
        }

        return $this;
    }

    /**
     * @return Collection|Bookings[]
     */
    public function getBookings(): Collection
    {
        return $this->Bookings;
    }

    public function addBooking(Bookings $booking): self
    {
        if (!$this->Bookings->contains($booking)) {
            $this->Bookings[] = $booking;
            $booking->setTime($this);
        }

        return $this;
    }

    public function removeBooking(Bookings $booking): self
    {
        if ($this->Bookings->contains($booking)) {
            $this->Bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getTime() === $this) {
                $booking->setTime(null);
            }
        }

        return $this;
    }
}

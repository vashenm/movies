<?php

namespace App\Entity;

use App\Repository\TheatresRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TheatresRepository::class)
 */
class Theatres
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Cinema::class, inversedBy="Theatres")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Cinema;

    /**
     * @ORM\ManyToMany(targetEntity=Movies::class, mappedBy="Theatre")
     */
    private $Movies;

    /**
     * @ORM\ManyToMany(targetEntity=Times::class, mappedBy="Theatre")
     */
    private $Times;

    /**
     * @ORM\OneToMany(targetEntity=Bookings::class, mappedBy="Theatre")
     */
    private $Bookings;

    public function __construct()
    {
        $this->Movies = new ArrayCollection();
        $this->Times = new ArrayCollection();
        $this->Bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCinema(): ?Cinema
    {
        return $this->Cinema;
    }

    public function setCinema(?Cinema $Cinema): self
    {
        $this->Cinema = $Cinema;

        return $this;
    }

    /**
     * @return Collection|Movies[]
     */
    public function getMovies(): Collection
    {
        return $this->Movies;
    }

    public function addMovie(Movies $movie): self
    {
        if (!$this->Movies->contains($movie)) {
            $this->Movies[] = $movie;
            $movie->addTheatre($this);
        }

        return $this;
    }

    public function removeMovie(Movies $movie): self
    {
        if ($this->Movies->contains($movie)) {
            $this->Movies->removeElement($movie);
            $movie->removeTheatre($this);
        }

        return $this;
    }

    /**
     * @return Collection|Times[]
     */
    public function getTimes(): Collection
    {
        return $this->Times;
    }

    public function addTime(Times $time): self
    {
        if (!$this->Times->contains($time)) {
            $this->Times[] = $time;
            $time->addTheatre($this);
        }

        return $this;
    }

    public function removeTime(Times $time): self
    {
        if ($this->Times->contains($time)) {
            $this->Times->removeElement($time);
            $time->removeTheatre($this);
        }

        return $this;
    }

    /**
     * @return Collection|Bookings[]
     */
    public function getBookings(): Collection
    {
        return $this->Bookings;
    }

    public function addBooking(Bookings $booking): self
    {
        if (!$this->Bookings->contains($booking)) {
            $this->Bookings[] = $booking;
            $booking->setTheatre($this);
        }

        return $this;
    }

    public function removeBooking(Bookings $booking): self
    {
        if ($this->Bookings->contains($booking)) {
            $this->Bookings->removeElement($booking);
            // set the owning side to null (unless already changed)
            if ($booking->getTheatre() === $this) {
                $booking->setTheatre(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\CinemaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CinemaRepository::class)
 */
class Cinema
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity=Theatres::class, mappedBy="Cinema", cascade={"persist", "remove"})
     */
    private $theatre;

    /**
     * @ORM\OneToMany(targetEntity=Theatres::class, mappedBy="Cinema")
     */
    private $Theatres;

    public function __construct()
    {
        $this->Theatres = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Theatres[]
     */
    public function getTheatres(): Collection
    {
        return $this->Theatres;
    }

    public function addTheatre(Theatres $theatre): self
    {
        if (!$this->Theatres->contains($theatre)) {
            $this->Theatres[] = $theatre;
            $theatre->setCinema($this);
        }

        return $this;
    }

    public function removeTheatre(Theatres $theatre): self
    {
        if ($this->Theatres->contains($theatre)) {
            $this->Theatres->removeElement($theatre);
            // set the owning side to null (unless already changed)
            if ($theatre->getCinema() === $this) {
                $theatre->setCinema(null);
            }
        }

        return $this;
    }
}

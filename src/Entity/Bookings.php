<?php

namespace App\Entity;

use App\Repository\BookingsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookingsRepository::class)
 */
class Bookings
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $BookingCode;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="Bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity=Movies::class, inversedBy="Bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Movie;

    /**
     * @ORM\ManyToOne(targetEntity=Times::class, inversedBy="Bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Time;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Theatres::class, inversedBy="Bookings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Theatre;

    /**
     * @ORM\Column(type="integer")
     */
    private $Seats;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBookingCode(): ?string
    {
        return $this->BookingCode;
    }

    public function setBookingCode(string $BookingCode): self
    {
        $this->BookingCode = $BookingCode;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getMovie(): ?Movies
    {
        return $this->Movie;
    }

    public function setMovie(?Movies $Movie): self
    {
        $this->Movie = $Movie;

        return $this;
    }

    public function getTime(): ?Times
    {
        return $this->Time;
    }

    public function setTime(?Times $Time): self
    {
        $this->Time = $Time;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTheatre(): ?Theatres
    {
        return $this->Theatre;
    }

    public function setTheatre(?Theatres $Theatre): self
    {
        $this->Theatre = $Theatre;

        return $this;
    }

    public function getSeats(): ?int
    {
        return $this->Seats;
    }

    public function setSeats(int $Seats): self
    {
        $this->Seats = $Seats;

        return $this;
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Movies;
use App\DataFixtures\TheatresFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MoviesFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $movie = new Movies();
        $movie->setName('Movie One');
        $movie->addTime($this->getReference(TimesFixtures::TIME_ONE));
        $movie->addTime($this->getReference(TimesFixtures::TIME_TWO));
        $movie->addTime($this->getReference(TimesFixtures::TIME_THREE));
        $movie->addTime($this->getReference(TimesFixtures::TIME_FOUR));
        $movie->addTime($this->getReference(TimesFixtures::TIME_FIVE));
        $movie->addTheatre($this->getReference(TheatresFixtures::THEATRE_ONE));
        $movie->addTheatre($this->getReference(TheatresFixtures::THEATRE_THREE));
        $manager->persist($movie);

        $movie2 = new Movies();
        $movie2->setName('Movie Two');
        $movie2->addTime($this->getReference(TimesFixtures::TIME_ONE));
        $movie2->addTime($this->getReference(TimesFixtures::TIME_TWO));
        $movie2->addTime($this->getReference(TimesFixtures::TIME_THREE));
        $movie2->addTime($this->getReference(TimesFixtures::TIME_FOUR));
        $movie2->addTime($this->getReference(TimesFixtures::TIME_FIVE));
        $movie2->addTheatre($this->getReference(TheatresFixtures::THEATRE_TWO));
        $movie2->addTheatre($this->getReference(TheatresFixtures::THEATRE_FOUR));
        $manager->persist($movie2);
        
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            TheatresFixtures::class,
        );
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Cinema;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CinemaFixtures extends Fixture
{
    public const CINEMA_ONE = 'Cinema-One';
    public const CINEMA_TWO = 'Cinema-Two';

    public function load(ObjectManager $manager)
    {
        $cinema = new Cinema();
        $cinema->setName('Cinema One');
        $manager->persist($cinema);

        $cinema2 = new Cinema();
        $cinema2->setName('Cinema Two');
        $manager->persist($cinema2);

        $manager->flush();

        $this->addReference(self::CINEMA_ONE, $cinema);
        $this->addReference(self::CINEMA_TWO, $cinema2);

    }

    public function getDependencies()
    {
        return array(
            TimesFixtures::class
        );
    }}

<?php

namespace App\DataFixtures;

use App\Entity\Times;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TimesFixtures extends Fixture
{
    public const TIME_ONE = 'TIME_ONE';
    public const TIME_TWO = 'TIME_TWO';
    public const TIME_THREE = 'TIME_THREE';
    public const TIME_FOUR = 'TIME_FOUR';
    public const TIME_FIVE = 'TIME_FIVE';


    public function load(ObjectManager $manager)
    {
        $time = new Times();
        $time->setTime('8:00-10:00');
        $manager->persist($time);

        $time2 = new Times();
        $time2->setTime('11:00-13:00');
        $manager->persist($time2);

        $time3 = new Times();
        $time3->setTime('14:00-16:00');
        $manager->persist($time3);


        $time4 = new Times();
        $time4->setTime('17:00-19:00');
        $manager->persist($time4);


        $time5 = new Times();
        $time5->setTime('20:00-22:00');
        $manager->persist($time5);
        
        $manager->flush();

        $this->addReference(self::TIME_ONE, $time);
        $this->addReference(self::TIME_TWO, $time2);
        $this->addReference(self::TIME_THREE, $time3);
        $this->addReference(self::TIME_FOUR, $time4);
        $this->addReference(self::TIME_FIVE, $time5);


    }
}

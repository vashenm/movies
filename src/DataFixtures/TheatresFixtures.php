<?php

namespace App\DataFixtures;

use App\Entity\Theatres;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class TheatresFixtures extends Fixture implements DependentFixtureInterface
{
    public const THEATRE_ONE = 'THEATRE_ONE';
    public const THEATRE_TWO = 'THEATRE_TWO';
    public const THEATRE_THREE = 'THEATRE_THREE';
    public const THEATRE_FOUR = 'THEATRE_FOUR';
    

    public function load(ObjectManager $manager)
    {
        $theatre = new Theatres();
        $theatre->setName('Theatre One');
        $theatre->setCinema($this->getReference(CinemaFixtures::CINEMA_ONE));
        $theatre->addTime($this->getReference(TimesFixtures::TIME_ONE));
        $theatre->addTime($this->getReference(TimesFixtures::TIME_TWO));
        $theatre->addTime($this->getReference(TimesFixtures::TIME_THREE));
        $theatre->addTime($this->getReference(TimesFixtures::TIME_FOUR));
        $theatre->addTime($this->getReference(TimesFixtures::TIME_FIVE));
        $manager->persist($theatre);

        $theatre2 = new Theatres();
        $theatre2->setName('Theatre Two');
        $theatre2->setCinema($this->getReference(CinemaFixtures::CINEMA_ONE));
        $theatre2->addTime($this->getReference(TimesFixtures::TIME_ONE));
        $theatre2->addTime($this->getReference(TimesFixtures::TIME_TWO));
        $theatre2->addTime($this->getReference(TimesFixtures::TIME_THREE));
        $theatre2->addTime($this->getReference(TimesFixtures::TIME_FOUR));
        $theatre2->addTime($this->getReference(TimesFixtures::TIME_FIVE));
        $manager->persist($theatre2);

        $theatre3 = new Theatres();
        $theatre3->setName('Theatre One');
        $theatre3->setCinema($this->getReference(CinemaFixtures::CINEMA_TWO));
        $theatre3->addTime($this->getReference(TimesFixtures::TIME_ONE));
        $theatre3->addTime($this->getReference(TimesFixtures::TIME_TWO));
        $theatre3->addTime($this->getReference(TimesFixtures::TIME_THREE));
        $theatre3->addTime($this->getReference(TimesFixtures::TIME_FOUR));
        $theatre3->addTime($this->getReference(TimesFixtures::TIME_FIVE));
        $manager->persist($theatre3);

        $theatre4 = new Theatres();
        $theatre4->setName('Theatre Two');
        $theatre4->setCinema($this->getReference(CinemaFixtures::CINEMA_TWO));
        $theatre4->addTime($this->getReference(TimesFixtures::TIME_ONE));
        $theatre4->addTime($this->getReference(TimesFixtures::TIME_TWO));
        $theatre4->addTime($this->getReference(TimesFixtures::TIME_THREE));
        $theatre4->addTime($this->getReference(TimesFixtures::TIME_FOUR));
        $theatre4->addTime($this->getReference(TimesFixtures::TIME_FIVE));
        $manager->persist($theatre4);

        $manager->flush();

        $this->addReference(self::THEATRE_ONE, $theatre);
        $this->addReference(self::THEATRE_TWO, $theatre2);
        $this->addReference(self::THEATRE_THREE, $theatre3);
        $this->addReference(self::THEATRE_FOUR, $theatre4);

    }

    public function getDependencies()
    {
        return array(
            CinemaFixtures::class,
            TimesFixtures::class
        );
    }
}

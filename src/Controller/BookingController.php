<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\BookingFormType;
use App\Entity\Bookings;
use App\Entity\Movies;
use App\Entity\Theatres;
use App\Entity\Times;
use App\Entity\Users;

class BookingController extends AbstractController
{
    /**
     * @Route("/booking/{mid}/{tid}/{timeId}", name="booking")
     */
    public function index($mid,$tid,$timeId)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();

        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movie = $repository->find($mid);

        $repository = $this->getDoctrine()->getRepository(Theatres::class);
        $theatre = $repository->find($tid);

        $repository = $this->getDoctrine()->getRepository(Times::class);
        $time = $repository->find($timeId);

        $form = $this->createForm(BookingFormType::class);


        return $this->render('booking/index.html.twig', [
            'movie' => $movie,
            'theatre' => $theatre,
            'time' => $time,
            'bookingForm' => $form->createView(),
        ]);
    }
}

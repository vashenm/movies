<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Movies;


class MovieController extends AbstractController
{
    /**
     * @Route("/movies", name="movie")
     */
    public function index()
    {
        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movies = $repository->findAll();

        return $this->render('movie/index.html.twig', [
            'movies' => $movies,
        ]);
    }

    /**
     * @Route("/movie/{id}", name="movie")
     */
    public function movie($id)
    {
        $repository = $this->getDoctrine()->getRepository(Movies::class);
        $movie = $repository->find($id);

        return $this->render('movie/single.html.twig', [
            'movie' => $movie,
        ]);
    }

}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200831204551 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bookings ADD theatre_id INT NOT NULL');
        $this->addSql('ALTER TABLE bookings ADD CONSTRAINT FK_7A853C35C80060CD FOREIGN KEY (theatre_id) REFERENCES theatres (id)');
        $this->addSql('CREATE INDEX IDX_7A853C35C80060CD ON bookings (theatre_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bookings DROP FOREIGN KEY FK_7A853C35C80060CD');
        $this->addSql('DROP INDEX IDX_7A853C35C80060CD ON bookings');
        $this->addSql('ALTER TABLE bookings DROP theatre_id');
    }
}

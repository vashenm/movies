<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200829103653 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movies (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE movies_theatres (movies_id INT NOT NULL, theatres_id INT NOT NULL, INDEX IDX_83D0193C53F590A4 (movies_id), INDEX IDX_83D0193C26C0348 (theatres_id), PRIMARY KEY(movies_id, theatres_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE times (id INT AUTO_INCREMENT NOT NULL, time VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE times_theatres (times_id INT NOT NULL, theatres_id INT NOT NULL, INDEX IDX_82731BE25624BEDC (times_id), INDEX IDX_82731BE226C0348 (theatres_id), PRIMARY KEY(times_id, theatres_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE times_movies (times_id INT NOT NULL, movies_id INT NOT NULL, INDEX IDX_C573DCCC5624BEDC (times_id), INDEX IDX_C573DCCC53F590A4 (movies_id), PRIMARY KEY(times_id, movies_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE movies_theatres ADD CONSTRAINT FK_83D0193C53F590A4 FOREIGN KEY (movies_id) REFERENCES movies (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE movies_theatres ADD CONSTRAINT FK_83D0193C26C0348 FOREIGN KEY (theatres_id) REFERENCES theatres (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE times_theatres ADD CONSTRAINT FK_82731BE25624BEDC FOREIGN KEY (times_id) REFERENCES times (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE times_theatres ADD CONSTRAINT FK_82731BE226C0348 FOREIGN KEY (theatres_id) REFERENCES theatres (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE times_movies ADD CONSTRAINT FK_C573DCCC5624BEDC FOREIGN KEY (times_id) REFERENCES times (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE times_movies ADD CONSTRAINT FK_C573DCCC53F590A4 FOREIGN KEY (movies_id) REFERENCES movies (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE theatres DROP INDEX UNIQ_6BA4B604B4CB84B6, ADD INDEX IDX_6BA4B604B4CB84B6 (cinema_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE movies_theatres DROP FOREIGN KEY FK_83D0193C53F590A4');
        $this->addSql('ALTER TABLE times_movies DROP FOREIGN KEY FK_C573DCCC53F590A4');
        $this->addSql('ALTER TABLE times_theatres DROP FOREIGN KEY FK_82731BE25624BEDC');
        $this->addSql('ALTER TABLE times_movies DROP FOREIGN KEY FK_C573DCCC5624BEDC');
        $this->addSql('DROP TABLE movies');
        $this->addSql('DROP TABLE movies_theatres');
        $this->addSql('DROP TABLE times');
        $this->addSql('DROP TABLE times_theatres');
        $this->addSql('DROP TABLE times_movies');
        $this->addSql('ALTER TABLE theatres DROP INDEX IDX_6BA4B604B4CB84B6, ADD UNIQUE INDEX UNIQ_6BA4B604B4CB84B6 (cinema_id)');
    }
}
